package com.pivot3.o11n.plugin;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class P3DomainTest {
	
    @SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(P3DomainTest.class);
    
    /*
     * Sample test. Do something cool here!
     */
    @Test
    public void testDefaultConstructor() {
    	P3Domain item = new P3Domain();
    	assertNotNull(item);
    }
}
