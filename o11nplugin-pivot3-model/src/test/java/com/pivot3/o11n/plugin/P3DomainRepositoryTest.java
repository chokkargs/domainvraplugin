package com.pivot3.o11n.plugin;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import ch.dunes.vso.sdk.endpoints.IEndpointConfigurationService;
import ch.dunes.vso.sdk.endpoints.IEndpointConfiguration;
import org.mockito.Mockito;

/** 
 * Unit tests for the P3DomainRepository class 
 */
public class P3DomainRepositoryTest {

    @Test
    public void testAddNewP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("attribute");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        P3Domain newP3Domain = repository.addP3Domain(item);
        
        assertTrue(repository.getP3Domains().size() == 1);
        assertEquals(item, newP3Domain);
    }

    @Test
    public void testAddInvalidP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        // item can't be null
        
        P3Domain item = null;
        
        try {
            repository.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("null") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
        
        // name is mandatory

        item = new P3Domain();

        try {
            repository.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
        
        // attribute1 is mandatory

        item.setipaddress("name");

        try {
            repository.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("attribute1") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
    }
    
    @Test
    public void testAddDuplicatedP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("8080");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        repository.addP3Domain(item);
        
        assertTrue(repository.getP3Domains().size() == 1);
        
        try {
            repository.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("already exists") > -1);
            assertTrue(repository.getP3Domains().size() == 1);
        }
    }
    
    @Test
    public void testUpdateP3Domain() {

        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("attribute");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        P3Domain newItem = repository.addP3Domain(item);
        
        assertTrue(repository.getP3Domains().size() == 1);
        assertEquals(item, newItem);
        assertNotNull(repository.getP3Domain("test"));
        
        newItem = new P3Domain();
        
        newItem.setipaddress("newTest");
        newItem.setportnumber("newAttribute");
        newItem.setusername("pivot3");
        newItem.setpassword("pivot3");
        
        P3Domain updatedItem = repository.updateP3Domain("test", newItem);
        
        assertTrue(repository.getP3Domains().size() == 1);
        assertEquals(newItem, updatedItem);
        assertNotNull(repository.getP3Domain("newTest"));
    }

    @Test
    public void testUpdateInvalidP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        // item can't be null
        
        P3Domain item = null;
        
        try {
            repository.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("null") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
        
        // name (from the item) is mandatory

        item = new P3Domain();

        try {
            repository.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
        
        // attribute1 is mandatory

        item.setipaddress("test");

        try {
            repository.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("attribute1") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }

        // name (to be updated) is mandatory

        item.setportnumber("attribute1");

        try {
            repository.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }

        // item should exists

        P3Domain updatedItem = repository.updateP3Domain("unknown", item);
        
        assertNull(updatedItem);
        assertTrue(repository.getP3Domains().isEmpty());
    }

    @Test
    public void testUpdateDuplicatedP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        P3Domain item1 = new P3Domain();
        item1.setipaddress("test1");
        item1.setportnumber("attribute");
        item1.setusername("pivot3");
        item1.setpassword("pivot3");
        repository.addP3Domain(item1);
        
        P3Domain item2 = new P3Domain();
        item2.setipaddress("test2");
        item2.setportnumber("attribute");
        item2.setusername("pivot3");
        item2.setpassword("pivot3");
        repository.addP3Domain(item2);

        assertTrue(repository.getP3Domains().size() == 2);

        item2.setipaddress("test1");
        try {
            repository.updateP3Domain("test2", item2);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("already exists") > -1);
            assertTrue(repository.getP3Domains().size() == 2);
        }
    }
    
    @Test
    public void testRemoveP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("attribute");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
		P3Domain newItem = repository.addP3Domain(item);
        
        assertTrue(repository.getP3Domains().size() == 1);
        assertEquals(item, newItem);
        
        P3Domain removedItem = repository.removeP3Domain("test");

        assertTrue(repository.getP3Domains().isEmpty());
        assertEquals(newItem, removedItem);
    }

    @Test
    public void testRemoveInvalidP3Domain() {
        
        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());

        // name is mandatory
        
        try {
            repository.removeP3Domain("");
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
        
        // item not found
        
        P3Domain removedP3Domain = repository.removeP3Domain("unknown");
        
        assertTrue(repository.getP3Domains().isEmpty());
        assertNull(removedP3Domain);
    }
    
    @Test
    public void testGetInvalidP3Domain() {

        P3DomainRepository repository = new P3DomainRepository();
        
        assertTrue(repository.getP3Domains().isEmpty());

        // name is mandatory
        
        try {
            repository.getP3Domain("");
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(repository.getP3Domains().isEmpty());
        }
        
        // item not found
        
        P3Domain removedItem = repository.getP3Domain("unknown");
        
        assertTrue(repository.getP3Domains().isEmpty());
        assertNull(removedItem);
    }

    @Test
    public void testSaveAndLoadP3Domains() throws IOException {

        P3DomainRepository repository = new P3DomainRepository();
        
        P3Domain item1 = new P3Domain();
        item1.setipaddress("test1");
        item1.setportnumber("attribute");
        item1.setusername("pivot3");
        item1.setpassword("pivot3");
        
        P3Domain item2 = new P3Domain();
        item2.setipaddress("test2");
        item2.setportnumber("attribute");
        item2.setusername("pivot3");
        item2.setpassword("pivot3");

        P3Domain item3 = new P3Domain();
        item3.setipaddress("test3");
        item3.setportnumber("attribute");
        item3.setusername("pivot3");
        item3.setpassword("pivot3");
        
        repository.addP3Domain(item1);
        repository.addP3Domain(item2);
        repository.addP3Domain(item3);
        
        assertTrue(repository.getP3Domains().size() == 3);

        IEndpointConfigurationService configurationService = Mockito.mock(IEndpointConfigurationService.class);
        Mockito.when(configurationService.getEndpointConfiguration(Mockito.anyString())).thenReturn(null);
        IEndpointConfiguration cfg = Mockito.mock(IEndpointConfiguration.class);
        Mockito.when(configurationService.newEndpointConfiguration(Mockito.anyString())).thenReturn(cfg);

        repository.saveP3Domains(configurationService);

        Mockito.verify(cfg).setString("name", "test1");
        Mockito.verify(cfg).setString("name", "test2");
        Mockito.verify(cfg).setString("name", "test3");
        Mockito.verify(cfg, Mockito.times(3)).setInt("attribute2", 0);
        Mockito.verify(cfg, Mockito.times(3)).setBoolean("attribute3", true);
        Mockito.verify(configurationService, Mockito.times(3)).saveEndpointConfiguration(cfg);

        repository.removeP3Domain(item1.getipaddress());
        repository.removeP3Domain(item2.getipaddress());
        repository.removeP3Domain(item3.getipaddress());
        
        assertTrue(repository.getP3Domains().isEmpty());

        Mockito.when(cfg.getString("name")).thenReturn("name1");
        Mockito.when(cfg.getString("attribute1")).thenReturn("attribute1");
        Mockito.when(configurationService.getEndpointConfigurations()).thenAnswer(invocationOnMock -> {
            List<IEndpointConfiguration> ret = new ArrayList<>();
            ret.add(cfg);
            return ret;
        });
        repository.loadP3Domains(configurationService);

        Mockito.verify(configurationService, Mockito.times(2)).getEndpointConfigurations();

        assertTrue(repository.getP3Domains().size() == 1);
    }
}
