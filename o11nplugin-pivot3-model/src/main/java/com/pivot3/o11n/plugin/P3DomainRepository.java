package com.pivot3.o11n.plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.dunes.vso.sdk.endpoints.IEndpointConfiguration;
import ch.dunes.vso.sdk.endpoints.IEndpointConfigurationService;

/** 
 * Repository for P3Domain objects.
 * It stores P3Domain objects and provides methods for loading from and saving to IEndpointConfigurationService.
 */
public class P3DomainRepository {

    private static final Logger log = LoggerFactory.getLogger(P3DomainRepository.class);
    
    private final Map<String, P3Domain> items = new ConcurrentHashMap<String, P3Domain>();

    private String configVersion = "";

    public P3DomainRepository() {
        log.info("P3DomainRepository created.");
    }
    
    /**
     * Adds a P3Domain object to the repository.
     * @param item P3Domain object to add
     * @return P3Domain object added.
     * @throws IllegalArgumentException Whether the P3Domain object is invalid or
     * it already exists in the repository.
     */
    public P3Domain addP3Domain(P3Domain item) throws IllegalArgumentException {
        String methodName = "addP3Domain";
    	log.info(methodName + "P3Domain " + item);
    	
        validateP3Domain(item);
        
        if (items.containsKey(item.getipaddress())) {
            throw new IllegalArgumentException("Item with name '" + item.getipaddress() + "' already exists!");
        }
        
        items.put(item.getipaddress(), item);
        
        return item;
    }
    
    /**
     * Updates the specified P3Domain from the repository. 
     * @param name Name of the P3Domain object to update
     * @param item P3Domain object with the updated data
     * @return P3Domain object updated.
     * @throws IllegalArgumentException Whether the P3Domain object is invalid or
     * it already exists in the repository (if the name has changed).
     */
    public P3Domain updateP3Domain(String name, P3Domain item) throws IllegalArgumentException {
        
        validateP3Domain(item);
        
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("'name' is empty!");
        }
        
        if (!items.containsKey(name)) {
            return null;
        }
        
        if (name.equals(item.getipaddress())) {
            items.put(name, item);
        } else {
            addP3Domain(item);
            items.remove(name);
        }
        
        return item;
    }
    
    /**
     * Removes a P3Domain object from the inventory.
     * @param name Name of the P3Domain object to remove
     * @return P3Domain object removed, or <code>null</code> if the repository contained
     * no mapping for the name.
     * @throws IllegalArgumentException Whether the name is empty.
     */
    public P3Domain removeP3Domain(String name) throws IllegalArgumentException {

        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("'name' is empty!");
        }
        
        return items.remove(name);
    }
    
    /**
     * Returns the list of P3Domain objects from the repository.
     * @return List of P3Domain objects from the repository.
     */
    public List<P3Domain> getP3Domains() {
        return new ArrayList<P3Domain>(items.values());
    }
    
    /**
     * Returns the P3Domain object from the inventory with the specified name.
     * @param name Name of the P3Domain object to get
     * @return P3Domain object, or <code>null</code> if the repository contained
     * no mapping for the name.
     * @throws IllegalArgumentException Whether the name is empty.
     */
    public P3Domain getP3Domain(String name) throws IllegalArgumentException {

        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("'name' is empty");
        }
        
        return items.get(name);
    }

    /**
     * Compares configuration version against the remote reprository.
     * @param configurationService IEndpointConfigurationService where to load the data from
     */
    public boolean isVersionChanged(IEndpointConfigurationService configurationService) {
        try {
            return !configVersion.equals(configurationService.getVersion());
        } catch (IOException e) {
            log.warn("Error reading configuration version.", e);
            return false;
        }
    }

    private P3Domain deserializeP3Domain(IEndpointConfiguration configurationItem) {
        P3Domain item = new P3Domain();
        item.setipaddress(configurationItem.getString("ipaddress"));
        item.setportnumber(configurationItem.getString("portnumber"));
        item.setusername(configurationItem.getString("username"));
        item.setpassword(configurationItem.getString("password"));
        return item;
    }

    private IEndpointConfiguration serializeP3Domain(IEndpointConfiguration configurationItem, P3Domain item) {
        configurationItem.setString("ipaddress", item.getipaddress());
        configurationItem.setString("portnumber", item.getportnumber());
        configurationItem.setString("username", item.getusername());
        configurationItem.setString("password", item.getpassword());
        return configurationItem;
    }

    /**
     * Loads the repository content from an IEndpointConfigurationService. The new content will replace the old one.
     * @param configurationService IEndpointConfigurationService where to load the data from
     */
    @SuppressWarnings("unchecked")
    public void loadP3Domains(IEndpointConfigurationService configurationService) {

        try {
            Collection<IEndpointConfiguration> endpointConfigurations = configurationService.getEndpointConfigurations();
            if (endpointConfigurations == null || endpointConfigurations.isEmpty()) {
                return;
            }
            configVersion = configurationService.getVersion();
            items.clear();
            for (IEndpointConfiguration configurationItem : endpointConfigurations) {
                addP3Domain(deserializeP3Domain(configurationItem));
            }
        } catch (IOException e) {
            log.error("Failed to load plugin configuration from endpoint service", e);
        } finally {
            log.info("Loaded configuration version {}", configVersion);
        }
    }

    private void deleteRemovedP3Domains(IEndpointConfigurationService configurationService) throws IOException {
        Collection<IEndpointConfiguration> endpointConfigurations = configurationService.getEndpointConfigurations();
        for (IEndpointConfiguration c : endpointConfigurations) {
            boolean found = false;
            for (P3Domain item : getP3Domains()) {
                if (item.getipaddress().equals(c.getId())) {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                log.info(String.format("Removing item %s", c.getId()));
                configurationService.deleteEndpointConfiguration(c.getId());
            }
        }
    }

    /**
     * Saves the repository content to an IEndpointConfigurationService.
     * @param configurationService IEndpointConfigurationService where to save the data to
     */
    public void saveP3Domains(IEndpointConfigurationService configurationService) {
        for (P3Domain item : getP3Domains()) {
            try {
                IEndpointConfiguration configurationItem = configurationService.getEndpointConfiguration(item.getipaddress());
                if(configurationItem == null) {
                    configurationItem = configurationService.newEndpointConfiguration(item.getipaddress());
                    log.info(String.format("Adding configuration for %s", item.getipaddress()));
                } else {
                    log.debug(String.format("Overwriting configuration for %s", item.getipaddress()));
                }
                configurationService.saveEndpointConfiguration(serializeP3Domain(configurationItem, item));
            } catch (IOException e) {
                log.error(String.format("Failed to save plugin configuration to endpoint service for item %s",
                            item.getipaddress()), e);
            }
        }
        try {
            deleteRemovedP3Domains(configurationService);
        } catch (IOException e) {
            log.error("Failed to remove plugin configuration items from endpoint service", e);
        }
    }
    
    /**
     * Validates a P3Domain object.
     * @param item P3Domain object to validate.
     * @throws IllegalArgumentException Whether the P3Domain object is invalid.
     */
    private void validateP3Domain(P3Domain item) throws IllegalArgumentException {
        
        if (item == null) {
            throw new IllegalArgumentException("P3Domain is null!");
        }
        
        if (StringUtils.isBlank(item.getipaddress())) {
            throw new IllegalArgumentException("'ipAddess' is empty");
        }
        
        if (StringUtils.isBlank(item.getportnumber())) {
            throw new IllegalArgumentException("'port' is empty");
        }
    }
}
