package com.pivot3.o11n.plugin;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the root element object type of the plug-in.
 */
public class P3Domain implements Serializable, Comparable<P3Domain> {
    
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(P3Domain.class);
    private String ipaddress;
    private String portnumber;
    private String username;
    private String password;
    
    public void setipaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getipaddress() {
		return ipaddress;
	}

	public void setportnumber(String portnumber) {
		this.portnumber = portnumber;
	}

	public String getportnumber() {
		return portnumber;
	}

	public void setusername(String username) {
		this.username = username;
	}

	public String getusername() {
		return username;
	}

	public void setpassword(String password) {
		this.password = password;
	}

	public String getpassword() {
		return password;
	}
	
	
	@Override
	public String toString() {
		return "P3Domain [ipaddress=" + ipaddress + ", portnumber=" + portnumber + ", username=" + username
				+ ", password=" + password + "]";
	}

	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        P3Domain other = (P3Domain) obj;
        return new EqualsBuilder()
            .appendSuper(super.equals(obj))
            .append(getipaddress(), other.getipaddress())
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(ipaddress)
            .append(portnumber)
            .append(username)
            .append(password)
            .toHashCode();
    }

    @Override
    public int compareTo(P3Domain other) {
        return getipaddress().compareTo(other.getipaddress());
    }
}
