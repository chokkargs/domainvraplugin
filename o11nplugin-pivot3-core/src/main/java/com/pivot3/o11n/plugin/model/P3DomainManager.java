package com.pivot3.o11n.plugin.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.dunes.vso.sdk.endpoints.IEndpointConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.dunes.vso.sdk.api.IPluginFactory;

import com.pivot3.o11n.plugin.P3Domain;
import com.pivot3.o11n.plugin.P3DomainRepository;
import com.pivot3.o11n.plugin.Pivot3PluginFactory;

/** 
 * Provides CRUD operations for P3Domain objects.
 * All the operations are thread-safe. 
 */
public class P3DomainManager {

    private static final Logger log = LoggerFactory.getLogger(P3DomainManager.class);
    
    private final P3DomainRepository repository = new P3DomainRepository();
    
    private final List<Pivot3PluginFactory> factories = Collections.synchronizedList(new ArrayList<Pivot3PluginFactory>());

    private IEndpointConfigurationService configurationService;

    private static class P3DomainManagerHolder {
        private static final P3DomainManager manager = new P3DomainManager();
    }
 
    public static final P3DomainManager getInstance() {
        return P3DomainManagerHolder.manager;
    }

    /**
     * Returns the singleton P3DomainManager to be used inside scripting. 
     * @param factory IPluginFactory implementation
     * @return P3DomainManager singleton.
     */
    public static P3DomainManager createScriptingSingleton(IPluginFactory factory) {
        return getInstance();
    }
    
    /**
     * Adds a Pivot3PluginFactory to the internal register for notification purposes.
     * @param factory Pivot3PluginFactory to add
     */
    public void addFactory(Pivot3PluginFactory factory) {
        factories.add(factory);
    }

    /**
     * Sets IEndpointConfigurationService instance to be used for plugin configuration repository.
     * @param configurationService IEndpointConfigurationService to be set
     */
    public void setEndpointConfigurationService(IEndpointConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * Removes a Pivot3PluginFactory from the internal register.
     * @param factory Pivot3PluginFactory to remove
     */
    public void removeFactory(Pivot3PluginFactory factory) {
        factories.remove(factory);
    }
    
    public P3Domain addP3Domain(P3Domain item) throws IllegalArgumentException, IOException {
        synchronized(repository) {
            P3Domain outItem = repository.addP3Domain(item);
            save();
            return outItem;
        }
    }

    public P3Domain updateP3Domain(String name, P3Domain item) throws IllegalArgumentException, IOException {
        synchronized(repository) {
            P3Domain outItem = repository.updateP3Domain(name, item);
            save();
            return outItem;
        }
    }
    
    public P3Domain removeP3Domain(String name) throws IllegalArgumentException, IOException {
        synchronized(repository) {
            P3Domain outItem = repository.removeP3Domain(name);
            save();
            return outItem;
        }
    }

    public List<P3Domain> getP3Domains() throws IOException {
        synchronized(repository) {
            reload();
            return repository.getP3Domains();
        }
    }
    
    public P3Domain getP3Domain(String name) throws IllegalArgumentException, IOException {
        synchronized(repository) {
            reload();
            return repository.getP3Domain(name);
        }
    }

    public void load() throws IOException {
        synchronized(repository) {
            repository.loadP3Domains(configurationService);
        }
    }

    public void save() throws IOException {
        synchronized(repository) {
            repository.saveP3Domains(configurationService);
            reload();
        }
        invalidateFactories();
    }

    private void reload() throws IOException {
    	synchronized(repository) {
    	    if (repository.isVersionChanged(configurationService)) {
                load();
            }
    	}
    }

    private void invalidateFactories() {
        for (Pivot3PluginFactory factory : new ArrayList<Pivot3PluginFactory>(factories)) {
            factory.invalidate("P3Domain", null);
            factory.getPluginNotificationHandler().notifyElementInvalidate(null, null);
        }
    }
}
