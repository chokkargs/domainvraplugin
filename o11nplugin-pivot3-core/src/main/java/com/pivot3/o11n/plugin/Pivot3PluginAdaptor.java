package com.pivot3.o11n.plugin;

import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pivot3.o11n.plugin.model.P3DomainManager;

import ch.dunes.vso.sdk.api.IPluginAdaptor;
import ch.dunes.vso.sdk.api.IPluginEventPublisher;
import ch.dunes.vso.sdk.api.IPluginFactory;
import ch.dunes.vso.sdk.api.IPluginNotificationHandler;
import ch.dunes.vso.sdk.api.IPluginPublisher;
import ch.dunes.vso.sdk.api.PluginLicense;
import ch.dunes.vso.sdk.api.PluginLicenseException;
import ch.dunes.vso.sdk.api.PluginWatcher;

import ch.dunes.vso.sdk.IServiceRegistryAdaptor;
import ch.dunes.vso.sdk.IServiceRegistry;
import ch.dunes.vso.sdk.endpoints.IEndpointConfigurationService;
import com.vmware.o11n.plugin.sdk.endpoints.extensions.EndpointConfigurationServiceFactory;

/**
 * Insert your comment for Pivot3PluginAdaptor here
 * @see <fill in related>
 *
 * @since   <Product revision>
 * @version <Implementation version of this type>
 * @author  <Author of this type>
 */
public class Pivot3PluginAdaptor implements IPluginAdaptor, IServiceRegistryAdaptor {

    private static final Logger log = LoggerFactory.getLogger(Pivot3PluginAdaptor.class);

    private String pluginName;
    private IPluginPublisher pluginPublisher;
    private IServiceRegistry serviceRegistry;

    @Override
    public IPluginFactory createPluginFactory(String sessionId, String username, String password, IPluginNotificationHandler pluginNotificationHandler) throws SecurityException, LoginException, PluginLicenseException {
        log.debug("createPluginFactory() --> sessionId: " + sessionId + ", username: " + username);
        
        Pivot3PluginFactory factory = new Pivot3PluginFactory(pluginName, sessionId, username, password, pluginNotificationHandler);
        
        P3DomainManager manager = P3DomainManager.getInstance();
        manager.addFactory(factory);

        return factory;

    }

    @Override
    public void setPluginName(String pluginName) {
        log.debug("setPluginName() --> pluginName: " + pluginName);
        this.pluginName = pluginName;
    }

    @Override
    public void setPluginPublisher(IPluginPublisher pluginPublisher) {
        log.debug("setPluginPublisher()");
        this.pluginPublisher = pluginPublisher;
    }

    @Override
    public void addWatcher(PluginWatcher pluginWatcher) {
        log.debug("addWatcher() --> pluginWatcher: " + pluginWatcher.getId());
    }

    @Override
    public void removeWatcher(String pluginWatcherId) {
        log.debug("removeWatcher() --> pluginWatcherId: " + pluginWatcherId);
    }

    @Override
    public void registerEventPublisher(String type, String id, IPluginEventPublisher pluginEventPublisher) {
        log.debug("registerEventPublisher() --> type: " + type + ", id: " + id);
    }

    @Override
    public void unregisterEventPublisher(String type, String id, IPluginEventPublisher pluginEventPublisher) {
        log.debug("unregisterEventPublisher() --> type: " + type + ", id: " + id);
    }

    @Override
    public void installLicenses(PluginLicense[] licenses) throws PluginLicenseException {
        log.debug("installLicenses()");
    }

    @Override
    public void uninstallPluginFactory(IPluginFactory pluginFactory) {
        log.debug("uninstallPluginFactory()");

        P3DomainManager manager = P3DomainManager.getInstance();
        manager.removeFactory((Pivot3PluginFactory) pluginFactory);
    }

    @Override
    public void setServiceRegistry(IServiceRegistry registry){
        serviceRegistry = registry;
        P3DomainManager manager = P3DomainManager.getInstance();
        IEndpointConfigurationService configurationService = EndpointConfigurationServiceFactory
                .lookupEndpointConfigurationService(serviceRegistry);
        manager.setEndpointConfigurationService(configurationService);
    }
}
