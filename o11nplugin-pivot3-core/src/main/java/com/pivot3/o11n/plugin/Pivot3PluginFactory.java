package com.pivot3.o11n.plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pivot3.o11n.plugin.model.P3DomainManager;

import ch.dunes.vso.sdk.api.HasChildrenResult;
import ch.dunes.vso.sdk.api.IPluginFactory;
import ch.dunes.vso.sdk.api.IPluginNotificationHandler;
import ch.dunes.vso.sdk.api.PluginExecutionException;
import ch.dunes.vso.sdk.api.QueryResult;

/**
 * Insert your comment for Pivot3PluginFactory here
 * @see <fill in related>
 *
 * @since   <Product revision>
 * @version <Implementation version of this type>
 * @author  <Author of this type>
 */
public class Pivot3PluginFactory implements IPluginFactory {

    private static final Logger log = LoggerFactory.getLogger(Pivot3PluginFactory.class);

    private String pluginName;
    private String sessionId;
    private String username;
    private String password;
    private IPluginNotificationHandler pluginNotificationHandler;

    private static final String ROOT_FINDER = "RootFinder";
    
    private Map<String, P3Domain> inventory = new HashMap<String, P3Domain>();
    
    public Pivot3PluginFactory(String pluginName, String sessionId, String username, String password, IPluginNotificationHandler pluginNotificationHandler) {
        log.debug("new Pivot3PluginFactory() --> username: " + username);
        this.pluginName = pluginName;
        this.sessionId = sessionId;
        this.username = username;
        this.password = password;
        this.pluginNotificationHandler = pluginNotificationHandler;

        refreshP3Domains();
    }

    @Override
    public void executePluginCommand(String cmd) throws PluginExecutionException {
        log.debug("executePluginCommand() --> cmd: " + cmd);
    }

    @Override
    public Object find(String type, String id) {
        log.debug("find() --> type: " + type + ", id: " + id);

        if ("P3Domain".equals(type)) {
            return inventory.get(id);
        }
        
        return null;
    }

    @Override
    public QueryResult findAll(String type, String query) {
        log.debug("findAll() --> type: " + type + ", query: " + query);

        if ("P3Domain".equals(type)) {
            return new QueryResult(findRelation(ROOT_FINDER, null, null));
        }

        return new QueryResult();
    }

    @Override
    public HasChildrenResult hasChildrenInRelation(String parentType, String parentId, String relationName) {
        log.debug("hasChildrenInRelation() --> parentType: " + parentType + ", parentId: " + parentId + ", relationName: " + relationName);

        if (ROOT_FINDER.equals(parentType)) {
            return inventory.isEmpty() ? HasChildrenResult.No : HasChildrenResult.Yes;
        }

        return HasChildrenResult.No;
    }

    @Override
    public List<?> findRelation(String parentType, String parentId, String relationName) {
        log.debug("findRelation() --> parentType: " + parentType + ", parentId: " + parentId + ", relationName: " + relationName);

        if (ROOT_FINDER.equals(parentType)) {
            List<P3Domain> items = new ArrayList<P3Domain>(inventory.values());
            Collections.sort(items);
            return items;
        }
        
        return null;
    }

    @Override
    public void invalidate(String type, String id) {
        log.debug("invalidate() --> type: " + type + ", id: " + id);
        
        if ("P3Domain".equals(type)) {
        	refreshP3Domains();
        }
    }

    @Override
    public void invalidateAll() {
        log.debug("invalidateAll()");
        refreshP3Domains();
    }
    
    public String getPluginName() {
        return pluginName;
    }

    public String getUsername() {
        return username;
    }

    public IPluginNotificationHandler getPluginNotificationHandler() {
        return pluginNotificationHandler;
    }

    /**
     * Reloads the P3Domain objects on the inventory from the plug-in configuration file.
     */
    private void refreshP3Domains() {
        
        inventory.clear();
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        try {
            for (P3Domain item: manager.getP3Domains()) {
                inventory.put(item.getipaddress(), item);
            }
        } catch (IOException e) {
            log.warn("Error reloading the plug-in configuration file: " + e.getMessage(), e);
        }

    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Pivot3PluginFactory factory = (Pivot3PluginFactory) obj;
        return new EqualsBuilder()
            .appendSuper(super.equals(obj))
            .append(pluginName, factory.getPluginName())
            .append(username, factory.getUsername())
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(pluginName)
            .append(sessionId)
            .append(username)
            .append(password)
            .toHashCode();
    }
}
