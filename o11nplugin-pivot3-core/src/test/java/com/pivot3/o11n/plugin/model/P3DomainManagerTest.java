package com.pivot3.o11n.plugin.model;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.BeforeClass;

import ch.dunes.vso.sdk.endpoints.IEndpointConfigurationService;
import ch.dunes.vso.sdk.endpoints.IEndpointConfiguration;
import org.mockito.Mockito;
import java.lang.reflect.Field;

import com.pivot3.o11n.plugin.P3Domain;

/** 
 * Unit tests for the P3DomainManager class 
 */
public class P3DomainManagerTest {

    @BeforeClass
    public static void beforeClass() {
        P3DomainManager manager = P3DomainManager.getInstance();
        IEndpointConfigurationService configurationService = Mockito.mock(IEndpointConfigurationService.class);
        manager.setEndpointConfigurationService(configurationService);
    }

    @Test
    public void testAddNewP3Domain() throws Exception {

        P3DomainManager manager = P3DomainManager.getInstance();

        Field field = manager.getClass().getDeclaredField("configurationService");
        field.setAccessible(true);
        IEndpointConfigurationService configurationService = (IEndpointConfigurationService) field.get(manager);
        IEndpointConfiguration cfg = Mockito.mock(IEndpointConfiguration.class);
        Mockito.when(configurationService.newEndpointConfiguration(Mockito.anyString())).thenReturn(cfg);

        assertTrue(manager.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("attribute");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        P3Domain newItem = manager.addP3Domain(item);
        
        assertTrue(manager.getP3Domains().size() == 1);
        assertEquals(item, newItem);
        
        // cleanup
        manager.removeP3Domain("test");
    }

    @Test
    public void testAddInvalidP3Domain() throws Exception {
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());
        
        // item can't be null
        
        P3Domain item = null;
        
        try {
            manager.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("null") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
        
        // name is mandatory

        item = new P3Domain();

        try {
            manager.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
        
        // attribute1 is mandatory

        item.setipaddress("test");

        try {
            manager.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("attribute1") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
    }
    
    @Test
    public void testAddDuplicatedP3Domain() throws Exception {
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("attribute");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        manager.addP3Domain(item);
        
        assertTrue(manager.getP3Domains().size() == 1);
        
        try {
            manager.addP3Domain(item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("already exists") > -1);
            assertTrue(manager.getP3Domains().size() == 1);
        }

        // cleanup
        manager.removeP3Domain("test");
    }
    
    @Test
    public void testUpdateP3Domain() throws Exception {

        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("8080");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        P3Domain newItem = manager.addP3Domain(item);
        
        assertTrue(manager.getP3Domains().size() == 1);
        assertEquals(item, newItem);
        assertNotNull(manager.getP3Domain("test"));
        
        newItem = new P3Domain();
        
        newItem.setipaddress("newTest");
        newItem.setportnumber("port");
        newItem.setusername("pivot3");
        newItem.setpassword("pivot3");
        
        P3Domain updatedItem = manager.updateP3Domain("test", newItem);
        
        assertTrue(manager.getP3Domains().size() == 1);
        assertEquals(newItem, updatedItem);
        assertNotNull(manager.getP3Domain("newTest"));

        // cleanup
        manager.removeP3Domain("newTest");
    }

    @Test
    public void testUpdateInvalidP3Domain() throws Exception {
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());
        
        // item can't be null
        
        P3Domain item = null;
        
        try {
            manager.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("null") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
        
        // name (from the item) is mandatory

        item = new P3Domain();

        try {
            manager.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
        
        // attribute1 is mandatory

        item.setipaddress("test");

        try {
            manager.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("attribute1") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }

        // name (to be updated) is mandatory

        item.setportnumber("attribute");

        try {
            manager.updateP3Domain("", item);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }

        // item should exists

        P3Domain updatedItem = manager.updateP3Domain("unknown", item);
        
        assertNull(updatedItem);
        assertTrue(manager.getP3Domains().isEmpty());
    }

    @Test
    public void testUpdateDuplicatedP3Domain() throws Exception {
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());
        
        P3Domain item1 = new P3Domain();
        item1.setipaddress("test1");
        item1.setportnumber("attribute");
        item1.setusername("pivot3");
        item1.setpassword("pivot3");
        manager.addP3Domain(item1);
        
        P3Domain item2 = new P3Domain();
        item2.setipaddress("test2");
        item2.setportnumber("attribute");
        item2.setusername("pivot3");
        item2.setpassword("pivot3");
        manager.addP3Domain(item2);

        assertTrue(manager.getP3Domains().size() == 2);

        item2.setipaddress("test1");
        try {
            manager.updateP3Domain("test2", item2);
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("already exists") > -1);
            assertTrue(manager.getP3Domains().size() == 2);
        }
        
        // cleanup
        manager.removeP3Domain("test1");
        manager.removeP3Domain("test2");
    }
    
    @Test
    public void testRemoveP3Domain() throws Exception {
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());
        
        P3Domain item = new P3Domain();
        item.setipaddress("test");
        item.setportnumber("attribute");
        item.setusername("pivot3");
        item.setpassword("pivot3");
        
        P3Domain newItem = manager.addP3Domain(item);
        
        assertTrue(manager.getP3Domains().size() == 1);
        assertEquals(item, newItem);
        
        P3Domain removedItem = manager.removeP3Domain("test");

        assertTrue(manager.getP3Domains().isEmpty());
        assertEquals(newItem, removedItem);
    }

    @Test
    public void testRemoveInvalidP3Domain() throws Exception {
        
        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());

        // name is mandatory
        
        try {
            manager.removeP3Domain("");
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
        
        // item not found
        
        P3Domain removedItem = manager.removeP3Domain("unknown");
        
        assertTrue(manager.getP3Domains().isEmpty());
        assertNull(removedItem);
    }
    
    @Test
    public void testGetInvalidP3Domain() throws Exception {

        P3DomainManager manager = P3DomainManager.getInstance();
        
        assertTrue(manager.getP3Domains().isEmpty());

        // name is mandatory
        
        try {
            manager.getP3Domain("");
            fail("it should throw an exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().indexOf("name") > -1);
            assertTrue(manager.getP3Domains().isEmpty());
        }
        
        // item not found
        
        P3Domain removedItem = manager.getP3Domain("unknown");
        
        assertTrue(manager.getP3Domains().isEmpty());
        assertNull(removedItem);
    }
}
